int clockPin = 13;
int latchPin = 10;
int dataPin = 12;
byte snake[8] = {
  0b0100010,
  0b0000110,
  0b0001100,
  0b1001000,
  0b1000010,
  0b0000011,
  0b0010001,
  0b0110000,
};
void setup() {
  // put your setup code here, to run once:
  pinMode(clockPin, OUTPUT);
  pinMode(dataPin, OUTPUT);
  pinMode(latchPin, OUTPUT);
  digitalWrite(latchPin, HIGH);
}

void loop() {
  // put your main code here, to run repeatedly:
    for (int i = 0; i < 8; i++) {
    digitalWrite(latchPin, HIGH);
    shiftOut(dataPin, clockPin, LSBFIRST, snake[i]);
    digitalWrite(latchPin, LOW);
    delay(120);
  }
}
